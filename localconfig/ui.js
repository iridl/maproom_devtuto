var uicoreConfig = uicoreConfig || {};
/* set google Analytics Id */
/* uicoreConfig.GoogleAnalyticsId='UA-432152-4'; */
/* enable helpmail button */
/* uicoreConfig.helpemail='help@iri.columbia.edu'; */
/* enable GoogleSearch */
/* uicoreConfig.GoogleSearchCX='002439769827255946943:6z9nl_kcvry'; */
/* DL site name */
/* uicoreConfig.SiteName="IRI Data Library"; */
/* Default resolution query server */
uicoreConfig.resolutionQueryServers["default"]= "/";

